
variable "vpc_cidr_block" {
  type        = string
}

variable "vpc_name" {
  type        = string
}

variable "instance_tenancy" {
  type        = string
  default     = "default"
}

variable "public_subnet_cidr_block_1" {
  type        = list
}

variable "private_subnet_cidr_block_1" {
  type        = list
}

variable "ports" {
  type = list
}

variable "sg_name" {
  type = string
  default = "default1"
}

variable "eks_cluster_role" {
  type = string
  default = "EKSCLUTERROLE_TF"
}

variable "eks_cluster_node_group_role" {
  type = string
  default = "EKSCLUTERNODEGROUPROLE_TF"
}

variable "eks_name" {
  type = string
}

variable "eks_node_grop_name" {
  type = string
  default = "Private_node"
}

variable "ec2_ami" {
  type = string
  default = "ami-068257025f72f470d"
}

variable "ec2_instance_type" {
  type = string
  default = "t2.micro"
}

variable "key_name" {
  type = string
  default = "dharmik_ap"
}
resource "aws_subnet" "my_vpc_subnet" {
  count = "${length(var.subnet_cidr_block)}"

  vpc_id     = var.vpc_id
  cidr_block = "${var.subnet_cidr_block[count.index]}"
  map_public_ip_on_launch = var.map_public_ip_on_launch

  tags = {
    Name = "${var.subnet_name}_${count.index}"
  }
} 

variable "vpc_id" {}

variable "subnet_cidr_block" {}

variable "map_public_ip_on_launch" {}

variable "subnet_name" {}
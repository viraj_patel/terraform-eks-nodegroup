resource "aws_eks_node_group" "example" {
  cluster_name    = var.eks_name
  node_group_name = var.eks_node_grop_name
  node_role_arn   = var.node_arn
  subnet_ids      = var.private_subnet_id.*.id

  scaling_config {
    desired_size = 1
    max_size     = 2
    min_size     = 1
  }

  update_config {
    max_unavailable = 2
  }

  labels = {
    type = "on-demand"
  }

}


resource "aws_eks_node_group" "example2" {
  cluster_name    = var.eks_name
  node_group_name = "${var.eks_node_grop_name}_2"
  node_role_arn   = var.node_arn
  subnet_ids      = var.private_subnet_id.*.id

  scaling_config {
    desired_size = 2
    max_size     = 5
    min_size     = 2
  }

  update_config {
    max_unavailable = 2
  }

  labels = {
    type = "spot"
  }

}

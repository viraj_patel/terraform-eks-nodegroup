resource "aws_eip" "elastic" {
  vpc = true
}

resource "aws_nat_gateway" "nat_gateway" {
  allocation_id = aws_eip.elastic.id
  subnet_id     = var.subnet_id.id

  tags = {
    Name = "${var.vpc_name}_NAT"
  }

}
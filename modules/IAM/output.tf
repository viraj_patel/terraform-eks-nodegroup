output "eks_cluster_arn" {
  value = aws_iam_role.EKSClusterRole.arn
}

output "eks_node_group_iam_arn" {
  value = aws_iam_role.NodeGroupRole.arn
}
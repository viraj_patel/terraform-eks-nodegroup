resource "aws_internet_gateway" "my_vpc_gw" {
  vpc_id = var.vpc_id

  tags = {
    Name = "${var.vpc_name}_internet_gateway"
  }
}
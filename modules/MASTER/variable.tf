variable "ec2_ami" {}
variable "ec2_instance_type" {}
variable "key_name" {}
variable "eks_name" {}
variable "public_subnet_id" {
  type = list
}
variable "sg_group" {}
resource "aws_instance" "ec2_public" {
  ami                         = var.ec2_ami
  associate_public_ip_address = true
  instance_type               = var.ec2_instance_type
  key_name                    = var.key_name
  subnet_id                   = var.public_subnet_id[0].id
  vpc_security_group_ids      = [var.sg_group]

  tags = {
    "Name" = "Master_EKS_${var.eks_name}"
  }

  # user_data = <<EOF
  # wget https://publicfiledownload.s3.ap-south-1.amazonaws.com/eks_helm_docker_aws.sh
  # bash eks_helm_docker_aws.sh

  # EOF

}
resource "aws_eks_cluster" "eks-cluster" {
  name     = var.eks_name
  role_arn = var.eks_cluster_arn
  version  = "1.22"

  vpc_config {
    subnet_ids          = flatten([var.private_subnet_id.*.id,var.public_subnet_id.*.id])
    security_group_ids  = flatten([var.vpc_sg])
  }

 
}
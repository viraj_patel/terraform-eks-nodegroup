variable "eks_name" {}

variable "eks_cluster_arn" {}

variable "public_subnet_id" {
  type        = list
}
variable "private_subnet_id" {
  type        = list
}

variable "vpc_sg" {
}
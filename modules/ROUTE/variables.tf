variable "vpc_id" {
}

variable "internet_gateway_id" {
}

variable "nat_gateway_id" {
}

variable "subnet_cidr_block_public" {
    type = list
}

variable "subnet_cidr_block_private" {
    type = list
}

variable "public_subnet_id" {
  type        = list
}

variable "private_subnet_id" {
  type        = list
}
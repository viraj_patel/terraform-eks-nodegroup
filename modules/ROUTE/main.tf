resource "aws_route_table" "route1" {
  vpc_id = var.vpc_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = var.internet_gateway_id
  }

  tags = {
    Name = "rt1"
  }
}

resource "aws_route_table" "route2" {
  vpc_id = var.vpc_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = var.nat_gateway_id
  }

  tags = {
    Name = "rt2"
  }
}

resource "aws_route_table_association" "a" {
  count = "${length(var.subnet_cidr_block_public)}"

  subnet_id      = "${element(var.public_subnet_id.*.id, count.index)}"
  route_table_id = aws_route_table.route1.id
}

resource "aws_route_table_association" "b" {
  count = "${length(var.subnet_cidr_block_private)}"

  subnet_id      = "${element(var.private_subnet_id.*.id, count.index)}"
  route_table_id = aws_route_table.route2.id
}
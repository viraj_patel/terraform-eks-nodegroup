vpc_cidr_block = "110.0.0.0/16"
vpc_name = "my-vpc"

public_subnet_cidr_block_1 = ["110.0.0.0/24","110.0.2.0/24"]
private_subnet_cidr_block_1 = ["110.0.1.0/24","110.0.3.0/24"]

ports         = [22, 80]

eks_cluster_role = "EKSClusterRole"
eks_cluster_node_group_role = "EKSNodeGroupRole"

eks_name = "test"

eks_node_grop_name = "PrivateNode"

ec2_ami = "ami-068257025f72f470d"
ec2_instance_type = "t2.micro"
key_name = "dharmik_ap"
module "vpc" {
  # path to the module directory
  source = "./modules/VPC"

  #arguments optional for the module. default values are provided in module. if you need to provide values, please uncomment vars.tf file
  vpc_cidr_block   = var.vpc_cidr_block
  instance_tenancy = var.instance_tenancy
  vpc_name         = var.vpc_name
}

module "public_subnet_1" {
  source = "./modules/SUBNET"

  vpc_id                  = module.vpc.instance_vpc_id
  subnet_cidr_block       = var.public_subnet_cidr_block_1
  map_public_ip_on_launch = true
  subnet_name             = "${var.vpc_name}_public_subnet_1"
}

module "private_subnet_1" {
  source = "./modules/SUBNET"

  vpc_id                  = module.vpc.instance_vpc_id
  subnet_cidr_block       = var.private_subnet_cidr_block_1
  map_public_ip_on_launch = false
  subnet_name             = "${var.vpc_name}_private_subnet_1"
}

module "internet_gateway" {
  source = "./modules/IG"

  vpc_id = module.vpc.instance_vpc_id
  vpc_name = var.vpc_name
}

module "nat_gateway" {
  source = "./modules/NAT"

  vpc_id = module.vpc.instance_vpc_id
  vpc_name = var.vpc_name
  subnet_id = module.public_subnet_1.subnet_id[0]
  depends_on = [
    module.internet_gateway
  ]
}

module "route" {
  source = "./modules/ROUTE"

  vpc_id              = module.vpc.instance_vpc_id
  internet_gateway_id = module.internet_gateway.vpc_internet_gateway_id
  nat_gateway_id = module.nat_gateway.nat_gateway_id
  subnet_cidr_block_public = var.public_subnet_cidr_block_1
  subnet_cidr_block_private = var.private_subnet_cidr_block_1
  public_subnet_id = module.public_subnet_1.subnet_id
  private_subnet_id = module.private_subnet_1.subnet_id
  depends_on = [
    module.public_subnet_1,
    module.private_subnet_1
  ]
}

module "sg" {
  source = "./modules/SG"
  vpc_id = module.vpc.instance_vpc_id
  sg_name = var.sg_name
  ports = var.ports
}


module "iam" {
  source = "./modules/IAM"

  eks_cluster_role = var.eks_cluster_role
  eks_cluster_node_group_role = var.eks_cluster_node_group_role
}
  
module "eks" {
  source = "./modules/EKS"

  eks_name = var.eks_name
  eks_cluster_arn = module.iam.eks_cluster_arn
  public_subnet_id = module.public_subnet_1.subnet_id
  private_subnet_id = module.private_subnet_1.subnet_id
  vpc_sg = module.sg.vpc_security_group_ids

   depends_on = [
    module.iam
  ]

}

module "node" {
  source = "./modules/NODE"

  eks_name = var.eks_name
  eks_node_grop_name = var.eks_node_grop_name
  node_arn = module.iam.eks_node_group_iam_arn
  private_subnet_id = module.private_subnet_1.subnet_id

  depends_on = [
    module.iam,
    module.eks
  ]
}

module "master" {
  source = "./modules/MASTER"

  ec2_ami = var.ec2_ami
  ec2_instance_type = var.ec2_instance_type
  eks_name = var.eks_name
  key_name = var.key_name
  public_subnet_id = module.public_subnet_1.subnet_id
  sg_group = module.sg.vpc_security_group_ids
  depends_on = [
    module.eks
  ]
}

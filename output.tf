output "SSH_COMMAND" {
  value = "ssh -i ${var.key_name}.pem ubuntu@${module.master.ssh_ip}"
}